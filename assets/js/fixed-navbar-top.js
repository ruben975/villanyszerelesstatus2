document.addEventListener("DOMContentLoaded", function(){
    window.addEventListener('scroll', function() {
        if (window.scrollY > 120) {
          document.getElementById('navbar_top').classList.add('fixed-top');
          document.getElementById('navbar_top').classList.add('bg-danger');
          // add padding top to show content behind navbar
          navbar_height = document.querySelector('.navbar').offsetHeight;
         
        } else {
          document.getElementById('navbar_top').classList.remove('fixed-top');
          document.getElementById('navbar_top').classList.remove('bg-danger');
           // remove padding top from body
          document.body.style.paddingTop = '0';
        } 
    });

    
  });
  
  
  if( window.innerWidth<1200)
  document.addEventListener("DOMContentLoaded", function(){
    window.addEventListener('scroll', function() {
        if (window.scrollY > 140) {
          document.getElementById('navbar_top2').classList.add('fixed-top');
          document.getElementById('navbar_top2').classList.add('bg-danger');
          // add padding top to show content behind navbar
          navbar_height = document.querySelector('.navbar').offsetHeight;
          document.body.style.paddingTop = navbar_height + 'px';
        } else {
          document.getElementById('navbar_top2').classList.remove('fixed-top');
         
           // remove padding top from body
          document.body.style.paddingTop = '0';
        } 
    });
  });
  
  
  if(window.innerWidth<768)
  document.addEventListener("DOMContentLoaded", function(){
    window.addEventListener('scroll', function() {
        if (window.scrollY > 0) {
          document.getElementById('navbar_top3').classList.add('fixed-top');
          document.getElementById('navbar_top3').classList.add('bg-danger');
          // add padding top to show content behind navbar
          navbar_height = document.querySelector('.navbar').offsetHeight;
          document.body.style.paddingTop = navbar_height + 'px';
        } else {
          document.getElementById('navbar_top3').classList.remove('fixed-top');
          document.getElementById('navbar_top3').classList.remove('bg-danger');
           // remove padding top from body
          document.body.style.paddingTop = '0';
        } 
    });
  });
  
  