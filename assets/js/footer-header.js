class MyHeader extends HTMLElement {
    connectedCallback(){
        this.innerHTML = 
    ` <!-- Header PC-->
    <div class="d-none d-xl-block">
  
      <div class="container mt-4 ">
        <div class="row ">
          <div class="col-9  ">
            <a href="index.html">
              <img class="mx-5" src="build/images/footer-logo.png" href="index.html"></a>
          </div>
          <div class="col-1 " style="text-align: right;">
            <i class="fas fa-phone-alt fa-2x my-2" style="color: white;"></i>
          </div>
          <div class="col-2 ">
            <b>
              <div class="row " style="color: white;">TELEFON</div>
            </b>
            <div class="row " style="color: white;">+36 30 719 3570</div>
          </div>
  
        </div>
      </div>
  
      <div class="container my-4">
        <!-- Navbar -->
        <nav id="navbar_top" class="navbar navbar-expand-lg  navbar-light ">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
  
            <div class="collapse navbar-collapse bg-danger" id="navbarNav">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 w-100">
                <li class="nav-item col mx-1 text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/rolunk.html" style="color: white;">Rólunk</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link active" aria-current="page" href="/villanyszerelesstatus2/villanyszereles.html"
                    style="color: white;">Villanyszerelés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/lakasfelujitas.html" style="color: white;">Lakásfelújítás</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/generalkivitelezes.html" style="color: white;">Generálkivitelezés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/referencia.html" style="color: white;">Referencia</a>
                </li>
                <li class="nav-item mx-1 col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/kapcsolat.html" style="color: white;">Kapcsolat</a>
                </li>
              </ul>
  
            </div>
          </div>
        </nav>
      </div>
    </div>
  
    <!-- Header tablet-->
    <div class="d-none d-md-block d-xl-none " tablet>
      <div class="container-fluid">
        <div class="container mt-4 ">
          <div class="row ">
            <div class="col-8  mx-4">
              <a href="index.html">
                <img class="" src="build/images/footer-logo.png" href="index.html"></a>
            </div>
            <div class="col-3  ">
              <div class="row ">
                <div class="col-3 " style="text-align: right;">
                  <i class="fas fa-phone-alt fa-2x my-2" style="color: white;"></i>
                </div>
                <div class="col-9 ">
                  <b>
                    <div class="row " style="color: white;">TELEFON</div>
                  </b>
                  <div class="row " style="color: white;">+36 30 719 3570</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  
      <div class="container my-4">
        <!-- Navbar -->
        <nav id="navbar_top2" class="navbar navbar-expand-lg  navbar-light bg-danger ">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
  
            <div class="collapse navbar-collapse bg-danger" id="navbarNav">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 w-100">
                <li class="nav-item col mx-1 text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/rolunk.html" style="color: white;">Rólunk</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link active" aria-current="page" href="/villanyszerelesstatus2/villanyszereles.html"
                    style="color: white;">Villanyszerelés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/lakasfelujitas.html" style="color: white;">Lakásfelújítás</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/generalkivitelezes.html" style="color: white;">Generálkivitelezés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/referencia.html" style="color: white;">Referencia</a>
                </li>
                <li class="nav-item mx-1 col text-center">
                  <a class="nav-link" href="/public/kapcsolat.html" style="color: white;">Kapcsolat</a>
                </li>
              </ul>
  
            </div>
          </div>
        </nav>
      </div>
    </div>
  
    <!-- Header phone-->
    <div class="d-md-none bg-danger" phone>
  
  
      <div class="container mb-4">
        <!-- Navbar -->
        <nav id="navbar_top3" class="navbar navbar-expand-lg  navbar-light bg-danger ">
          <div class="container-fluid d-flex justify-content-center">
            <button class="navbar-toggler text-center" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <img class="mx-5" src="build/images/footer-logo-white.png"></a>
              <span class="navbar-toggler-icon"></span>
            </button>
  
            <div class="collapse navbar-collapse bg-danger" id="navbarNav">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 w-100">
                <li class="nav-item col mx-1 text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/rolunk.html" style="color: white;">Rólunk</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link active" aria-current="page" href="/villanyszerelesstatus2/villanyszereles.html"
                    style="color: white;">Villanyszerelés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/lakasfelujitas.html" style="color: white;">Lakásfelújítás</a>
                </li>
                <li class="nav-item col  text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/generalkivitelezes.html" style="color: white;">Generálkivitelezés</a>
                </li>
                <li class="nav-item col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/referencia.html" style="color: white;">Referencia</a>
                </li>
                <li class="nav-item mx-1 col text-center">
                  <a class="nav-link" href="/villanyszerelesstatus2/kapcsolat.html" style="color: white;">Kapcsolat</a>
                </li>
              </ul>
  
            </div>
          </div>
        </nav>
      </div>
    </div>
        `
    }
}

class MyFooter extends HTMLElement {
    connectedCallback(){
        this.innerHTML = 
    `<div class="footer" style="background-color: rgb(43, 43, 43);">
    <div class="container">
      <div class="row">
        <div class="col text-center  "><a href="index.html"><img src="build/images/footer-logo.png" alt="" style="padding-top: 40px;"
              class="my-1"></a></div>
        <div class="col-sm-12 col-md-4 text-center " style="color: white; ">
          <p style="padding-top: 60px;"">📞 +36 30 719 3570</p></div>
    <div class=" col ">
      <div class=" container text-center" style="color: white;padding-top: 20px;">
            <div class="row">
              <div class="col"><a class="nav-link" href="/public/rolunk.html" style="color: white;">Rólunk</a></div>
              <div class="col"><a class="nav-link" href="/public/villanyszereles.html"
                  style="color: white;">Villanyszerelés</a></div>
            </div>
            <div class="row">
              <div class="col"><a class="nav-link" href="/public/lakasfelujitas.html" style="color: white;">Lakásfelújítás</a>
              </div>
              <div class="col"><a class="nav-link" href="/public/generalkivitelezes.html"
                  style="color: white;">Generálkivitelezés</a></div>
            </div>
            <div class="row">
              <div class="col"><a class="nav-link" href="/public/referencia.html" style="color: white;">Referencia</a></div>
              <div class="col" style="padding-bottom: 20px;"><a class="nav-link" href="/public/kapcsolat.html"
                  style="color: white;">Kapcsolat</a></div>
            </div>
        </div>
      </div>
    </div>
  </div>
  </div>
    `
    }
}
customElements.define('my-header', MyHeader)

customElements.define('my-footer', MyFooter)